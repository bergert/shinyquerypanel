
# Function for module UI
update_ui <- function(id) {
  ns <- NS(id)
  
  tabPanel(title="Read Dataset Codelists", 
           fluidRow(
             column(4,
                    actionButton(ns("updateEBX5"), "Update Codelists From EBX5")),
             column(8,
                    textOutput(ns("EBX5_last_update")))
           )
  )
}

# Function for module server
update_server <- function(input, output, session,update,query) {
  ns<-session$ns  
  
  observe({
    #lastUpdate<-read.csv("update/lastUpdate.csv")
    update$ebx<-file.info(paste0('codelists/Timeseries_',query$timeserie,'.RData'))$mtime
  })
  
  observeEvent(input$updateEBX5,{
    showModal(modalDialog(
      title = "Update Codelists from EBX5",
      "Codelists are updating and app will restart, this action can take several minutes, please wait ...",
      easyClose = TRUE,
      footer = NULL
    ))
    metadata <- ReadMetadata()
    root<-getwd()
    setwd(paste0(root,"/codelists"))
    ReadDatasetCodelists(metadata, query$timeserie)
    removeModal()
    setwd(root)
    #update$ebx<-Sys.time()
    #lastUpdate<-read.csv("update/lastUpdate.csv")
    #lastUpdate[lastUpdate$timeserie==query$timeserie,2]<-update$ebx
    #write.csv(lastUpdate,"update/lastUpdate.csv",row.names = F)
    js$restart()
  })
  
  output$EBX5_last_update<-renderText({
    sprintf('Last update : %s',update$ebx)})
}