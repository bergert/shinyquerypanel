//#######################################################################
//# country-chooser.js
//#
//# implements the java-script for the right-left browser
//#######################################################################
// https://shiny.rstudio.com/articles/building-inputs.html

(function() {

var options = [];
jQuery.fn.filterByText = function(textbox, selectSingleMatch) {
  return this.each(function() {
    var select = this;
    options = [];
    $(select).find('option').each(function() {
      options.push({value: $(this).val(), text: $(this).text()});
    });
    $(select).data('options', options);
    $(textbox).bind('change keyup', function() {
      options = $(select).empty().scrollTop(0).data('options');
      var search = $.trim($(this).val());
      var regex = new RegExp(search,'gi');

      $.each(options, function(i) {
        var option = options[i];
        if(option.text.match(regex) !== null) {
          $(select).append(
             $('<option>').text(option.text).val(option.value)
          );
        }
      });
      if (selectSingleMatch === true && 
          $(select).children().length === 1) {
        $(select).children().get(0).selected = true;
      }
    });
  });
};

function updateChooser(chooser) {
    chooser = $(chooser);
    var left = chooser.find("select.left");
    var right = chooser.find("select.right");
    var leftArrow = chooser.find(".left-arrow");
    var rightArrow = chooser.find(".right-arrow");

    var canMoveTo = (left.val() || []).length > 0;
    var canMoveFrom = (right.val() || []).length > 0;

    leftArrow.toggleClass("muted", !canMoveFrom);
    rightArrow.toggleClass("muted", !canMoveTo);
}

function sortMeBy(arg, sel, elem, order) {
        var $selector = sel,
        $element = $selector.children(elem);
        $element.sort(function(a, b) {
                var an = parseInt(a.getAttribute(arg)),
                bn = parseInt(b.getAttribute(arg));
                if (order == "asc") {
                        if (an > bn)
                        return 1;
                        if (an < bn)
                        return -1;
                } else if (order == "desc") {
                        if (an < bn)
                        return 1;
                        if (an > bn)
                        return -1;
                }
                return 0;
        });
        $element.detach().appendTo($selector);
}

function move(chooser, source, dest) {
    chooser = $(chooser);
    var selected = chooser.find(source).children("option:selected");
    var dest2 = chooser.find(dest);
    dest2.children("option:selected").each(function(i, e) {e.selected = false;});
    dest2.append(selected);
    sortMeBy("value", dest2, "option", "asc");
    updateChooser(chooser);
    chooser.trigger("change");
}


function moveall(chooser, source, dest) {
    chooser = $(chooser);
    var selected = chooser.find(source).children();
    var dest2 = chooser.find(dest);
    dest2.children().each(function(i, e) {e.selected = false;});
    dest2.append(selected);
    updateChooser(chooser);
    chooser.trigger("change");
}

$(".chooser").change(function(){

});

$(document).on("change", ".chooser select", function() {
    updateChooser($(this).parents(".chooser"));
    console.log(". chooser select", $(this).parents(".chooser"));
});


// right arrow
$(document).on("click", ".chooser .right-arrow", function() {
    move($(this).parents(".chooser"), ".left", ".right");
});

$(document).on("click", ".chooser .right-arrow-all", function() {
   console.log("right-arrow-all");
   moveall($(this).parents(".chooser"), ".left", ".right");
});


// left arrow
$(document).on("click", ".chooser .left-arrow", function() {
    move($(this).parents(".chooser"), ".right", ".left");
});

$(document).on("click", ".chooser .left-arrow-all", function() {
   console.log("left-arrow-all");
   moveall($(this).parents(".chooser"), ".right", ".left");
});



$(document).on("dblclick", ".chooser select.left", function() {
    move($(this).parents(".chooser"), ".left", ".right");
});

$(document).on("dblclick", ".chooser select.right", function() {
    move($(this).parents(".chooser"), ".right", ".left");
});

var binding = new Shiny.InputBinding();

binding.find = function(scope) {
    return $(scope).find(".chooser");
};

binding.initialize = function(el) {
    updateChooser(el);
    $(function() {
      $('.left').filterByText($('.chooser-input-left'), true);
    }); 
};

binding.getValue = function(el) {
  return {
    left:  $.makeArray($(el).find("select.left option").map(function(i, e)  { return e.value; })),
    right: $.makeArray($(el).find("select.right option").map(function(i, e) { return e.value; }))
  };
};

binding.setValue = function(el, value) {
// TODO: implement
};

binding.subscribe = function(el, callback) {
    $(el).on("change.chooserBinding", function(e) {
        callback();
    });
};

binding.unsubscribe = function(el) {
    $(el).off(".chooserBinding");
};

binding.getType = function() {
return "country.chooser";
};

Shiny.inputBindings.register(binding, "COUNTRY.chooser");

})();